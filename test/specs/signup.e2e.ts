import { expect, browser, $ } from "@wdio/globals";

describe("My Login application", () => {
  it("should create an account", async () => {
    await browser.url(`http://localhost:5173/compte/inscription`);

    const email = `email${Date.now()}@test.com`;

    await $("#inputFullName").setValue("tomsmith");
    await $("#inputEmail").setValue(email);
    await $("#inputPassword").setValue("SuperSecretPassword!");
    await $('button[type="submit"]').click();

    await browser.pause(1000);

    const loggedInElement = await $(
      ".dropdown-toggle.btn.btn-outline-secondary"
    );
    const loggedInText = await loggedInElement.getText();
    expect(loggedInText).toContain("tomsmith");
  });

  it("should fail to create an account when email is invalid", async () => {
    const accountDropdown = await $(
      ".dropdown-toggle.btn.btn-outline-secondary"
    );
    accountDropdown.click();
    await $(".signout-btn").click();
    await browser.url(`http://localhost:5173/compte/inscription`);

    await $("#inputFullName").setValue("tomsmith");
    await $("#inputPassword").setValue("SuperSecretPassword!");
    await $('button[type="submit"]').click();

    await browser.pause(1000);

    const errorElement = await $(".alert.alert-danger");
    const errorText = await errorElement.getText();
    expect(errorText).toContain("Request failed with status code 400");
  });
});

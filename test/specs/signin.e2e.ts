import { expect, browser, $ } from "@wdio/globals";

describe("My Login application", () => {
  it("should log into your account when existing credentials", async () => {
    await browser.url(`http://localhost:5173/compte/connexion`);

    const defaultEmail = "user@test.com";
    const defaultPassword = ";>jep]A_";
    const defaultFullName = "Mollie Brady";

    await $("#inputEmail").setValue(defaultEmail);
    await $("#inputPassword").setValue(defaultPassword);
    await $('button[type="submit"]').click();

    await browser.pause(1000);

    const loggedInElement = await $(
      ".dropdown-toggle.btn.btn-outline-secondary"
    );
    const loggedInText = await loggedInElement.getText();
    expect(loggedInText).toContain(defaultFullName);
  });

  it("should fail to log in with invalid credentials", async () => {
    const accountDropdown = await $(
      ".dropdown-toggle.btn.btn-outline-secondary"
    );
    accountDropdown.click();
    await $(".signout-btn").click();
    await browser.url(`http://localhost:5173/compte/inscription`);

    await $("#inputFullName").setValue(
      "cetemailnestpasutilisejensuissur@non.com"
    );
    await $("#inputPassword").setValue("SuperSecretPassword!");
    await $('button[type="submit"]').click();

    await browser.pause(1000);

    const errorElement = await $(".alert.alert-danger");
    const errorText = await errorElement.getText();
    expect(errorText).toContain("Request failed with status code 400");
  });
});
